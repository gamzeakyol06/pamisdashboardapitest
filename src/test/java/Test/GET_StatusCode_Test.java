package Test;
import Base.Base;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GET_StatusCode_Test extends Base {
    String  token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);

    //ProductionResultType
    @Test
    public void Test_ProductionResultType() {

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                when().
                get(PRODUCTION_PAGE_URL + "ProductionResultType").
                then().
                statusCode(200).log().all();
    }

    //ProductionTag
    @Test
    public void Test_ProductionTag() {


    given().headers("Authorization","Bearer "+token).
        contentType("application/json").
        when().
        get(PRODUCTION_PAGE_URL + "ProductionTag").
        then().
        statusCode(200).log().all();
        }
        }