package Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import Base.Base;
import static io.restassured.RestAssured.given;

public class PUT_ProductionResult_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){

        map.put("id",7);
        map.put("resultName","Test Result Updated");
        map.put("isOK",true);
        map.put("isActive", true);

        System.out.println(map);
    }

    @Test(priority = 1,description = "200 Success")
    public void PUT_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                put(PRODUCTION_PAGE_URL + "ProductionResultType/Update?id="+7).
                then().
                statusCode(204).log().all();
    }

    @Test (priority = 2)
    public void PUT_Update_Assert_Test() throws InterruptedException, IOException {

        Response response = doGetRequest(PRODUCTION_PAGE_URL+"ProductionResultType");
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        List<String> jsonResponse_name = doGetResponseResultName(response);

        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postNameID = jsonResponse_listid.get(i);
            System.out.println(postNameID);
            if(postNameID == 7 ) {
                String postNameData = jsonResponse_name.get(i);
                System.out.println(postNameData);
                System.out.println("Result Name " + postNameData);

                Assert.assertEquals(postNameData, map.get("resultName"));
                System.out.println(postNameData);
                System.out.println(map.get("resultName"));
            }
        }

    }

}
